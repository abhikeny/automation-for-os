"""
File Name:                mac_firefox.py
Created by (Author/s):    A. Keny
Date of Creation:         12/22/2012
Date of Modification:     12/22/2012
Description:              This autoscript will:
                          --Check current Firefox version on Mac
                          --Check latest available Firefox for Mac
                          --Download Firefox installer (if new is available)
                          --Install Firefox (if new is downloaded)
Notes:                    

"""

# ==============================================================================
# Import Python Modules Here
# ==============================================================================
pass
# ==============================================================================
# Import Framework Modules Here
# ==============================================================================
from modules.object_autoscript import AutoScript
# ==============================================================================
# Declare Global Variables Here
# ==============================================================================
pass
# ==============================================================================
# Declare Main Autoscript Class Here
# ==============================================================================
class Mac_Firefox(AutoScript):
    
    def call(self):
        # ======================================================================
        # Define Autoscript Functionality Here
        # ======================================================================
        self.description('x')
    
    def setup(self):
        # ======================================================================
        # Define Autoscript Setup Parameters Here
        # ======================================================================
        pass
    
    def run(self):
        # ======================================================================
        # Define Autoscript Tasks Here
        # ======================================================================
        pass
    
    def cleanup(self):
        # ======================================================================
        # Define Autoscript Teardown Tasks Here
        # ======================================================================
        pass
    
# ==============================================================================
# End of Autoscript.
# ==============================================================================