"""
File Name:                win_firefox.py
Created by (Author/s):    A. Keny
Date of Creation:         01/21/2013
Date of Modification:     01/21/2013
Description:              This autoscript will:
                          --Check current Firefox version on Windows
                          --Check latest available Firefox for Windows
                          --Download Firefox installer (if new is available)
                          --Install Firefox (if new is downloaded)
Notes:                    

"""

# ==============================================================================
# Import Python Modules Here
# ==============================================================================
pass
# ==============================================================================
# Import Framework Modules Here
# ==============================================================================
from modules.object_autoscript import AutoScript
# ==============================================================================
# Declare Global Variables Here
# ==============================================================================
pass
# ==============================================================================
# Declare Main AutoScript Class Here
# ==============================================================================
class Win_Firefox(AutoScript):
    
    def call(self):
        # ======================================================================
        # Define Autoscript Functionality Here
        # ======================================================================
        pass
    def setup(self):
        # ======================================================================
        # Define Autoscript Setup Parameters Here
        # ======================================================================
        pass
    def run(self):
        # ======================================================================
        # Define Autoscript Tasks Here
        # ======================================================================
        pass
    def cleanup(self):
        # ======================================================================
        # Define Autoscript Teardown Tasks Here
        # ======================================================================
        pass
# ==============================================================================
# End of Autoscript.
# ==============================================================================